# Comunicazione

## Middleware


Middleware 
:   livello sw che astrae da rete, hw, os e linguaggi. Fornisce un
    modello computazionale e di comunicazione uniforme.

Tale livello fornisce servizi alle applicazioni, questi sono
indipendenti dall'applicazione in questione.

In genere i servizi offerti sono:

-   comunicazione
-   meccanismi di sicurezza
-   transazioni
-   recovery
-   gestione di risorse condivise

## Chiamata remota a procedura (RPC)

Ovvero un programma chiama una procedura su una macchina remota.

-   il client chiama la procedura, la sue esecuzione è sospesa
-   viene inviata una richiesta al server
-   il server invoca le procedure richieste
-   il risultato è inviato al client, che riprende l'esecuzione

### Passaggio dei parametri

L'operazione che prevede il passaggio dei parametri tramite un messaggio
si chiama *parameter marshaling*. Ci sono però due problemi da risolvere
riguardo tale operazione:

1.  client e server possono avere rappresentazioni diverse dei dati
    (little vs big endian)
2.  client e server devono usare la stessa codifica

Le soluzioni sono:

1.  trasformare i dati in un formato indipendente dalla macchina
2.  client e server devono essere d'accordo sulla codifica da usare
    (come XML)

#### Puntatori

Nel caso di puntatori non possiamo semplicemente passare un riferimento, 
in quanto questo ha valore locale.

Le soluzione a questo priblema sono diverse:

#. copiare l'intera struttura a cui punta il riferimento
	- facile per strutture piatte
	- difficile per strutture nidificate
#. riferimenti globali
	- i riferimenti hanno valore sia sul client che sul server
	- quando i parametri sono passati a una procedura remota, 
		possono semplicemente essere copiati da una macchina all'altra
	- usata tipicamente in sistemi OO

Nei sistemi OO ci sono sia locali che remoti, questi sono trattati differentemente:

- locali: copiati e trasmessi
- remoti: solo lo stub viene chiamato e trasmesso

#### Supporto

Il meccanismo RPC viene fornito tramite

- framework: indipendenti dai linguaggi ma poco trasparenti
- costrutti: trasmarenti al programmatore, ma legati ad un linguaggio, 
	client e server devono essere scritti nello stesso linguaggio

### Versione asincrona

- dopo la richiesta, il client non attende il risultato della procedura, 
	invece continua l'esecuzione; aspetterà la risposta dopo
- dopo la ricezione della richiesta, il server invia un acknowledgment
	al client

### Versione con callback

- il client implementa una procedure per permettere al server di comunicargli 
	la risposta; si chiama **callback**
- il client di comporta come nel caso asincrono, ma include nella richiesta la 
	procedura da usare
- il server si comporta come nel caso asincrono, usando però il metodo di callback

### Versione multicast

- il client invia una richiesta a molteplici server, che la processano in 
	parallelo e indipendentemente
- per quanto rigarda le risposte:
	- o solo la prima viene accettata
	- o le risposte di tutti i server vengono fuse tra loro

### Binding client-server

Nelle applicazioni reali esiste un'altro stadio, ovvero il binding.
Avremo quindi un **registry** o **directory server** che mantiene le informazioni 
circa i vari server, di solito un mapping tra i loro nomi e i loro indirizzi.
Il server quindi si deve registrare in modo tale che il client lo possa 
trovare all'interno del registro, usando il suo nome simbolico.

## Middleware orientato ai messaggi (MoM)

I componenti comunicano tramite lo scambio reciproco di messaggi.
Questa primitiva fornisce una comunicazione persistente e asincrona in quanto 
gli gli attori coninvolti in uno scambio di messaggi non devono essere necessariamente 
attivi durante la trasmmissione; il middleware fornisce infatti un servizio di 
archiviazione.

- ogni applicazione ha una coda locale che mantiene i messaggi inviati e 
	ricevuti
- una coda può essere condivisa tra più applicazioni
- un messagio che parte dall coda del mittente, passa attraverso una serie 
	di server di comunicazione fino a che la coda di destinazione non è 
	raggiunta
- la comunicazione avviene per inserimenti e rimozioni di messaggi dalle code

### Messaggi

- i messaggi possono contenere ogni tipo di dato e possono essere di qualsiasi 
	dimensione, entro un certo limite
- il middleware può supportare la frammentazione dei messaggi
- la destinazione, e la relativa coda, è identificata da un nome a livello di 
	sistema
- ogni messaggio contiene il nome/indirizzo del suo destinatario

### API

metodo	azione
-----	------
put		appende un messaggio alla coda
get		rimuove il primo messaggio dalla coda (bloccante)
poll	come pull ma non bloccante
notify	installa un handler da chiamare alla ricezione dei messaggi

### Gestore della coda

- può essere una linked library o un processo a sè
- ogni applicazione è associata ad un coda locale, è quindi presente un gestore 
- un'applicazione può inviare/ricevere messaggi da/per la coda locale 

### Indirizzi

- ogni nome logico è associato ad un indirizzo:porta
- ogni gestore di coda mantiene una tabella di traduzione

### Gestione della Messaging Queue (MQ)

- collegare i MQ tramite un overlay network
	- creare canali di comunicazione
	- gestire routing e tabelle di traduzione
- mantenere l'overlay network a seguito di cambiamenti (come nuovi MQ)

### Message Broker

Componente che si comporta come un gateway.
Invece di avere una componente che usa più protocolli per comunicare 
con destinatari diversi, abbiamo il message broker, questo:

1. converte i messaggi in modo che possano essere capiti da applicaioni diverse 
2. incontra i destinatari sullo stile del modello publish-subscribe

