# Introduzione

## Sistema Distribuito

Un sistema distribuito è un insieme di elementi computazionali autonomi,
che appaiono all'utente come un singolo sistema coerente (dove avviene
la computazione, dove sono i dati, ecc, diventa irrilevante).


Elemento computazionale autonomo

:   processo sw o dispositivo hw. +

    -   Caratteristiche:

        -   ogni nodo è autonomo e con una propria nozione di tempo

        -   le componenti interagiscono tramite messaggi

Overlay network

:   ogni nodo del sistema comunica solo con i suoi vicini. L'insieme dei
    vicini di una componente può essere statico o dinamico. +


    Gruppo aperto

    :   ogni nodo può entrare nel sistema e comunicare con gli altri.

    Gruppo chiuso

    :   un meccanismo è richiesto per far entrare nuovi nodi nel
        sistema.

L'overlay network è costruito sopra la rete fisica; ciò implica che un
arco nella prima potrebbe essere un percorso nella seconda. Solitamente
l'overlay network è connessa.

## Obiettivi di design

### Condivisione delle risorse

Rendere possibile accedere a risorse remote e condividerle in modo
controlato ma efficente. Le risorse possono essere qualsiasi cosa:
dispositivi, potenza di calcolo, file e dati.

### Trasparenza di distribuzione

Fornire un'interfaccia utente che nasconda il fatto che processi e
risorse sono distribuiti nella rete. Infatti si dice che un sistema è
trasparente se appare all'utente finale come un singolo sistema.

Tale proprietà può essere applicata a diverse entità:

-   Accesso: rappresentazione e accesso delle risorse

-   Location: dove sono le risorse

-   Migrazione: come sono spostate le risorse

-   Riallocazione: come una risorsa è spostata mentre è in uso

-   Replicazione: come una risorsa è replicata in un'atra location

-   Concorrenza: come una risorsa viene condivisa tra utenti in
    concorrenza

-   Fallimento: fallimento e recupero di una risorsa

Nel realizzare tale proprietà, ci scontriamo con la realtà in quanto:

-   ritardi e latenze non possono essere nascosti

-   i nodi possono fallire

-   determinare se una risorsa ha subito un fallimento è lento e
    difficile

-   le operazioni influenzano le performance: alcune operazioni sono
    molto costose

-   ci sono problemi di concorrenza

### Openness

Un sistema si dice aperto se:

-   coopera e si integra bene con altri sistemi

-   nuove risorse possono essere aggiunte dinamicamente e rese
    disponibili ad altri utenti

-   le risorse sono offerte sulla base dei protocolli standard

Le sue proprietà sono:

-   Interfacce standard

-   Interoperabilità

-   Portabilità

-   Estensibilità

### Flessibilità 

caratteristica è fornita dalla separazione tra meccanismo e politica.
	
politica
:	specifica cosa deve essere fatto 
	
meccanismo
:	specifica come deve essere fatto

### Scalabilità

Capacità di un sistema di crescere e gestire una quantità crescente di
lavoro.

Questa può essere misurata in diverse dimensioni:

-   Dimensionale: # di utenti, nodi, processi

-   Amministrativa: # di organizzazioni o domini amministrativi che
    condividono un sistema distribuito

-   Di carico: capacità di espandere e contrarre le risorse per gestire
    più o meno carico

-   Funzionale: abilità di supportare nuove funzionalità con il minimo
    sforzo

-   Geografica: abilità di mantenere buone performance e usabilità,
    indipendentemente dalla distanza tra i nodi

### Availability

Ogni richiesta ricevuta da un nodo funzionante, deve risultare in una
risposta. Non c'è limite superiore di tempo per la ricezione della
risposta.

Assumiamo che:

1.  i fallimenti sono rilevati il prima possibile

2.  le procedure di recupero iniziano appena un fallimento viene
    rilevato

### Modularity

-   l'applicazione è formata da un insieme di parti, i moduli

-   Ogni modulo ha:

    -   un'interfaccia che definisce l'interazione con gli altri moduli

    -   una specifica funzionale ma indipendente dall'implementazione

### Sfide

Di per se, raggiungere gli obiettivi appena descritti è complicato. Si
aggiungono poi fattori aggiuntivi che devono essere tenuti in
considerazione:

-   Sicurezza

-   Sfide implementative

-   QoS

