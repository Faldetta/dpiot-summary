# Modello ad attori

Modello matematico riguardo la concorrenza computazionale

attori
:	primitiva universale della computazione

Il modello definisce delle regole riguardo il comportamento e l'interazione 
degli attori.

## Terminologia

Messaggo 
:	unità di comunicazione. Fatta di un tipo ed un payload.

Mailbox
:	lista ordinata di messaggi. L'ordine denota l'arrivo dei messaggi, 
	senza implicare che i messaggi siano processati nel medesimo ordine

Turn
:	elaborazione che consiste nel prendere un messaggio dalla mailbox per 
	processarlo 

Interfaccia
:	definisce lista e tipi di messaggi ai quali reagire

Comportamento
:	combinazione di una lista di messaggi e instruzioni di completamento

## Attore

Primitiva unitaria della computazione, incapsula:

- stato
- comportamento
- comunicazione

Quando un attore riceve un messaggio lo usa per effettuare una computazione di qualche 
tipo. Poi può inviare un messaggio ad altri attori.

Si noti che non c'è una nozione di canale per la trasmissione dei messaggi.

### Comportamento di base

Quando un messaggio arriva, un attore può:

- creare un numero finito di nuovi attori (**create**)
- inviare messaggi agli altri attori (compreso se stesso, in caso si vogliano 
	implementare comportamnti ricorsivi) (**send**)
- cambiare il suo comportamento aggiornando il suo stato interno; la modifica 
	ha effetto alla ricezione del messaggio seguente (**become**)

### Proprietà

#### Isolamento

Gli attori sono isolati tra loro e non condividono la memoria, quindi il loro 
stato non può essere cambiato direttamente da un altro attore.
Gli attori non passano mai riderimenti/puntatori, o qualsiasi altra forma di 
dato condiviso, all'interno di un messaggio; solo dati immutabili e indirizzi 
sono ammessi.
Ciò garantice la sincronizzazione implicita.

#### Computazione sequenziale

Gli attori hanno una *mailbox* per salvare i messaggi al loro arrivo.

- le operazioni sulla mailbox sono atomiche
- un attore processa un dato messaggio sequenzialmente, ovvero se 
	un attore riceve 3 messaggi da un medesimo interlocutore, ne eseguirà 
	solo uno alla volta
- il calcolo su di un oggetto nonpuò contere operazioni bloccanti

#### Principio del turno (turn) isolato

Questo principio garantisce che il modello ad attori è libero da deadlock 
e concorrenza sui dati a basso livello.
Ciò vale fintanto che si parla della computazione di un singolo messaggio.

### Sistemi di attori

Più attori in esecuzione nello stesso momento prendono il noem di sistema 
(system). 

Tutto è un attore, per comuncare necessitano di indirizzi, che siano sullo stesso 
nodo o che siano distribuiti.

#### Indirizzi

- gli indirizzi sono nomi logici, ma non sono identità (un attore può avere più 
	indirizzi)
- la relazione attori-indirizzi è molti a molti
	- un indirizzo a più attori = service replication
	- un attore con più indirizzi = service aliasing

Si noti che la mapaptura degli indirizi non è parte del modello matematico, 
bensì fa parte dell'implementazione.

#### Messaggi

- inviati asincronicamente e salvati nelle mailbox (la send non è bloccante)
- si assume che i messaggi siano trasmessi con *best effort* 
	- non ci sono garanzie
	- c'è un limite speriore per il tempo di ricezione

## Vantaggi

- gli attori sono primitive ti concorrenza leggera che scalano naturalmente 
	su molte macchine
- l'isolamento degli attori permette ai programmatori di ragionare localmente 
- gli attori sono entità lascamente associate, ciò dipende solo da messaggi di 
	input e output; il comportamento di alcuni attori può essere cambiato senza 
	impattare sull'intero sistema

## Gestione dei fallimenti

- gli attori hanno un buon supporto per la gestione dei fallimenti e il recupero 
	da questi
	- fondamentale in sistemi distribuiti, in cui i nodi possono fallire
- **IDEA**: alcuni nodi si comportano da supervisori e monitorano gli altri nodi
- i supervisori possono essere organizzati in modo gerarchico per gestire 
	i fallimenti in isolamento

### Stategie di recovery

Ci sono quattro strategie che un supervisore può adottare quando sorge un problema 
con i suoi sottoposti:

#. ignorare l'errore e far riprendere il lavoro al sottoposto
#. riavviare il sottoposto per resettare il suo stato
#. bloccare il sottoposto
#. far scalare il problema al proprio supervisore (ovvero il supervisore 
del supervisore)
