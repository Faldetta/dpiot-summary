# Algoritmi distribuiti: modello e computazioni di base

## Modello

### Eventi

Sono importanti in quanto il comportamento delle entità è reattivo.

I possibili eventi sono:

- originato dal sistema
	- clock tick
	- ricezione di un messaggio
- esterno
	- impulso esterno

### Azioni

Un'azione altro non è che una sequenza di attività, ma con qualche proprietà:

- è atomica
- è finita
- può essere `nil`

### Comportamento

#### Comportamento delle entità

Insieme di regole (algoritmo o protocollo) per un entità per tutti i possibili 
eventi e (X) tutti i possibli stati.

#### Comportamento del sistema

Equivale al comportamento di tutte le entità.
Si  dice *simmetrico* o *omogeneo* se tutte le entità hanno 
lo stesso comportamento.

Proprietà
: ogni sistema può essere realizzato simmetricamente.

### Comunicazione

L'unità di comunicazione è un **messaggio**, ovvero una sequenza finita di bit.

#### Modello point-to-point

Un entità può inviare e ricevere messaggi solamente ai suoi vicini.

##### Assiomi 

Tempo di trasmissione finito
: in assenza di guasti, un messaggio da un'entità ad un suo vicino raggiunge 
	quest'ultimo in tempo finito

Orientamento locale
: Ogni entità distingue tra i suoi vicini

##### Restrizioni

###### Di comunicazione

Ordine dei messaggi
: in assenza di fallimenti, i messaggi trasmessi tramite lo stesso 
	collegamento arrivano in ordine di invio

Si noti che ci posono essere collegamenti bidirezionali.

###### Rilevamento dei guasti

Rilevamento del fallimento di un arco
: un entità può rilevare se un collegamento ha fallito se è stato 
	ripristinato
	
Rilevamento del fallimento di un'entità
: un entità può rilevare se un vicino fallisce

###### Reliability

Garanzia di consegna
: ogni messaggio inviato sarà ricevuto senza compromissioni

Reliability parziale
: non ci saranno fallimenti

Reliability totale
: non ci sono stati e non ci saranno fallimenti

###### Topologiche

Il grafo delle entità è fortemente connesso.

###### Conoscenza

- numero di nodi
- numero di archi
- diametro

###### Tempo

Ritardo
: esiste una costante che, in assenza di fallimenti, rappresenta il massimo 
	ritardo di comunicazione

Sincronizzazione degli orologi
: tutti gli orologi sono incrementati di un'unità simultaneamente a intervalli 
	costanti

###### Complessità/Performance

- attività di comunicazione (pov sistema)
	- $M$: numero di messaggi in bit
	- carico delle entità: $M/|V|$
	- carico dei collegamenti: $M/|E|$
- tempo (pov user)
	- idealmente 1 unità temporale per trasmettere un messaggio
	- i ritardi sono impredicibili

## Conoscenza

### Scope

- locale
- implicita: qualcuno in un gruppo
- esplicita: tutti in un gruppo
- comune: tutti in gruppo sanno tutto e che 
	gli altri sanno

### Tipo

- topologico: tipo di struttura di appartenenza
- metrico: quantità sulla struttura di appartenenza
- senso di direzione: direzione dei flussi per collegamento
	o nodo

Maggiore è la conscenza disponibile, minore è la portabilità dell'algoritmo.

### Importanza

Per esempio, la conoscenza topologica ci permette di ridurre la complessità di un algoritmo.

Se prendiamo ad esempio un ipercubo di dimensione k ($2^k$ vertici), ogni nodo 
ha $n \cdot k/2$ collegamenti ($O(n \cdot log(n))$).
Un flooding normale costerebbe $O(n \cdot log(n))$, poichè tra ogni coppia di 
nodi esiste un percorso unico con numero di label decrescente:

- ogni entità riceve l'informazione una sola volta e invia l'informazione 
	una sola volta
- complessità temporale: $2m-(n-1)=O(n^2)$ 
- complessità in messaggi: $(n-1)$
