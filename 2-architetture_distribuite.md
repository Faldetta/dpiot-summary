# Architetture distribuite

L'organizzazione di un sistema distribuito può essere divisa in

-   architettura software: componenti del sistema e relative interazioni

-   architettura di sistema: identifica i dispositivi che costituiscono
    il sistema e la loro interconnettività; descrive come il software è
    istanziato sull'hardware

Un'architettura si riduce a quattro domande essenziali:

1.  Quali componenti comunicano?

2.  Quale paradigma di comunicazione è usato?

3.  Quali sono i ruoli e le responsabilità delle componenti?

4.  Come sono organizzate le componenti?

## Architetture Software

### Architettura a strati

-   le componenti sono organizzate in strati

-   una componente può delegare una certa operazione al livello
    sottostante

-   una componente può notificare un risultato o un evento ad una
    componente soprastante

-   ogni livello implementa un protocollo di comunicazione

-   ogni livello fornisce un'interfaccia che permette di utilizzare il
    servizio fornito

### Architettura basata sugli oggetti

-   le componenti sono oggetti che interagiscono tramite chiamate a
    metodi

-   gli oggetti sono su macchine diverse

-   gli oggetti incapsulano lo stato e offrono un interfaccia che
    nasconde l'implementazione

-   un oggetto deve poter essere rimpiazzabile con un altro con la
    medesima interfaccia

### Architettura basata sui servizi (SOA)

-   le componenti di un servizio interagiscono attraverso la rete
    mediante un protocollo

-   un servizio

    -   rappresenta un'attvità

    -   è una black-box

    -   è autocontenuto

    -   può essere fatto di altri servizi

    -   fornisce un interfaccia

    -   è riusabile

-   i servizi possono usare diverse tecnologie sottostanti

-   un'applicazione distribuita può essere vista come una composizione
    di servizi che operano in armonia (come un OS)

#### Entità delle SOA


Broker

:   rende le informazioni sui servizi note ai consumer

Provider

:   rende disponibile un servizio e fornisce al broker le informazioni
    che lo riguardano

Consumer

:   identifica il servizio nel registro del broker e lo associa ad un
    servizio fornito da un provider per usarlo

### Architettura basata sulle risorse (REST)

-   un sistema distribuito è un insieme di risorse, controllate dalle
    componenti

-   le risorse possono essere aggiunte, rimosse e recuperate da
    componenti remote

#### Caratteristiche principali

-   le risorse sono identificate d URI

-   ogni servizio offre la stessa interfaccia, formata dai metodi HTTP

-   l'esecuzione è *stateless*

-   i messaggi si autodescrivono, ovvero devono avere abbastanza
    informazioni da poter capire come processare il contenuto

-   una risorsa può essere un singleton o una collezione
    `https://api.example.com/resources`
    `https://api.example.com/resources/item`

-   una risorsa può contenere risorse, gerarchicamente

Poichè i servizi REST si appoggiano a HTTP, questi usano i suoi metodi:

+-----------------------------------+-----------------------------------+
| metodo                            | descrizione                       |
+===================================+===================================+
| PUT                               | crea una nuova risorsa            |
+-----------------------------------+-----------------------------------+
| GET                               | recupera lo stato di una risorsa  |
+-----------------------------------+-----------------------------------+
| DELETE                            | rimuovi una risorsa               |
+-----------------------------------+-----------------------------------+
| POST                              | recupera una risorsa              |
+-----------------------------------+-----------------------------------+

### Architettura basata sugli eventi / Publish-Subscribe

Questa architettura prevede tre componenti:


Publisher

:   invia messaggi in broadcast senza avere conoscenza dei destinatari

Subscriber

:   resta in attesa di messaggi (o eventi) di suo interesse senza avere
    alcuna conoscenza sui publisher

Broker

:   inoltrano i messaggi dei publisher ai subscriber

    -   publisher e subscriber interagiscono solamente tramite i broker

    -   i broker tracciano gli eventi di interesse per i subscriber

    -   un evento prodotto da un publisher può essere ricevuto da molti
        subscriber

    	filtri
		: I subscriber possono esprimere interesse in un evento (o
          pattern di eventi) sotto forma di un filtro. Questi filtri
          possono essere aggiunti e rimossi tramite apposite
          primitive. I filtri possono essere di tre tipologie:

			Basato sui Topic
			:   gli argomenti fungono da filtro

			Basato sui Contenuti
			:   tutti i dati pubblicati sono filtri espressi sotto forma di
				predicati espressi su attributi

			Basato sui Tipi
			:   gli eventi sono oggetti appartenenti ad uno specifico tipo, che può
				incapsulare attributie metodi per formare un filtro sul tipo

### Tuple space

Le componenti comunicano tramite tuple che vengono condivise e salvate
in uno spazio condiviso.

-   le componenti inseriscono le tuple nello spazio condiviso

-   per recuperare un tupla, una componente fornisce un pattern che
    viene confrontato con le tuple, le tuple che matchano vengono
    restituite

-   lo spazio condiviso è persistente, le tuple sono savate fino a che
    non sono esplicitamente rimosse

-   i creatori e i consumatori di tuple non devono coesistere
    contemporaneamente

- integrazione con gli eventi

	-   una componente si iscrive alle tuple che soddisfano un certo pattern

	-   quando una tupla entra nel tuple-space, i subscriber iscritti
    	saranno notificati

## Architetture di sistema

### Client-Server

È formata da:

-   componenti che offrono un servizio, i **server**

-   componenti che usano un servizio, i **client**

Tipicamente client e server sono su macchine diverse, la loro
interazione si avvale del modello **request-reply**.

Le componenti di un'applicazione sono:

-   presentation layer

-   processing layer

-   data layer

In funzione di come sono separate queste componenti, possiamo parlare
di:


single-tier

:   configurazione a mainframe, l'applicazione è sul server, il
    terminale permette di accedervi

two-tier

:   i tre layer sono divisi tra le due macchine, questo può essere fatto
    in diversi modi, al variare delle percentuali su client e su
    server 


three-tier

:   ogni layer giace su una diversa macchina (client, application 
    server, + db server)

### Peer-to-Peer

In questo caso non c'è distinzione tra client e server. Ogni
partecipante del sistema contribuisce al sistema condividendo le proprie
risorse, in questo modo queste sono condivise tra i partecipanti con lo
scopo di completare un obiettivo. Le componenti coinvolte in una
specifica attività, o applicazione, interagiscono tra loro come pari,
ovvero **peer**.

L'overlay network può essere:

-   non strutturata

-   strutturata

-   gerarchica

La gestione delle risorse dipende solo dal tipo di overlay network.

#### Non strutturata

-   ogni nodo mantiene una lista di vicini

-   quando un nodo entra nella rete, contatta un peer noto per avere una
    lista iniziale di peer (ne verranno scelti solo alcuni)

-   la lista si aggiorna in funzione delle indisponibilità nei peer

-   la topologia ricorda un *random graph*

Poichè le risorse sono distribuite tra i peer, cercare una risorsa
equivale a cercare il peer che la possiede. Tuttavia un peer conosce
solo:

-   i suoi vicini

-   le risorse di cui i vicini sono responsabili

In questo caso abbiamo due strategie per trovare una risorsa:

-   flooding

-   random walk

##### Flooding

Un nodo inoltra la sua richiesta a tutti i suoi vicini. Quando un nodo
riceve una query:

-   se ha già visto quella query, la scarta

-   se è responsabile per la risorsa richiesta risponde al mittente

-   altrimenti inoltra la query a tutti i suoi vicini

Note:

-   semplice da implementare

-   richiede TTL per i pacchetti

-   non scala, troppi messaggi vengono scambiati

##### Random Walk

Un nodo invia la richiesta per una risorsa ad un vicino random. Quando
un nodo riceve una query:

-   se è responsabile per la risorsa risponde

-   altrimenti inoltra la richiesta ad un vicino random

Note:

-   semplice da implementare

-   più cammini random possono essere iniziati simultaneamente

-   richiede TTL per i pacchetti

#### Strutturata

-   la rete ha una topologia nota (e.g. anello, albero, ...)

-   ogni risorsa del sistema è associata univocamente ad una chiave

-   la chiave è solitamente restituita con l'hash della risorsa

-   ogni peer ha un id calcolato mediante funzioni hash

-   ogni nodo è responsabile per i dati associati con un sottoinsieme di
    chiavi

Come cerchiamo una risorsa? Tramite la chiave

-   il sistema fornisce una funzione *lookup* che mappa una chiave ad un
    peer

-   l'implementazione della funzione di lookup traccia la risorsa al
    peer che la possiede

-   una buona funzione richiede $(O(log(N)))$ messaggi, con $N$
    numero di nodi

#### Gerarchica

-   i peer sono classificati in *super-peer* e *weak-peer*

-   ogni weak è connesso ad un super

-   tutte e comunicazioni da e per un weak passano per il relativo super

-   l'associazione super-weak può essere fissa o dinamica

