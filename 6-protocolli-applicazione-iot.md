# Protocolli applicazione per l'IoT

## MQTT

Message Queuing Telementry Transport e' un protocollo publish/subscribe leggero 
ed affidabile.

Le sue caratteristiche principali sono:

- architettura client-server
- semplice a lato client (ovviamente la complessita e' a lato server)
- fornisce QoS

Esempi di uso ne sono:

- satelliti
- domotica
- e-health
- ...

MQTT si avvale del modello publisher-subscriber.

### Modello publish-subscribe

Il publisher ed il subscriber sono entrambi client, che non si conoscono tra loro.
L'event service, anche detto broker, e' il server; quest'ultimo conosce sia publisher che subscriber.

Questo modello fa si che publisher e subscriber siano completamente separati per quanto riguarda:

tempo
: P e S possono non essere in esecuzione in contemporanea

spazio
: non necessitano di conoscersi l'un l'altro

sincronizzazione
: le operazioni non sono interrotte durante la pubblicazione e la ricezione; 
    ovviamente si  intende in P e S

L'event service ricevera' i messaggi dai publisher, li filtrerà e li inoltrerà ai subscriber (
di cui gestisce le varie iscrizioni e disiscrizioni).
Le operazioni di filtraggio possono essere:

- basate sul topic
- basate sul contenuto
- basate sul tipo

Un notevole vantaggio, rispetto alla classica architettura client-server, è la scalabilità; questa 
richiede la parallelizzazione del broker.

### Publish-subscribe in MQTT

- P e S conoscono il broker
- spesso i messaggi sono consegnati quasi in real-time ma 
    - il broker può salvare i messaggi per i S offline
    - i S offline devono essere connessi ad una sessione persistente
    - i S devono essere, ovviamente, iscritti al topic
- ottimo per dispositivi low-power in quanto la complessità è sul server, 
    segue che l'utilizzo sui client è semplice
- filtraggio basato sui topic
    - richiede una gerarchia di topic ben progettata, anche in vista di sviluppi 
        futuri
- QoS
    - asicura la reliability della consegna dei messaggi
    - 3 livelli (0, 1, 2)
    
### Connessione

1. il client invia un CONNECT che deve contenere il *ClientID* (contien anche altri parameti
    facoltativi)
2. il broker conferma la connessione tramite CONNECTACK, questo contiene un codice che 
    comunica al client l'avvenuta connessione o altrimenti il motivo per cui questa non
    è riuscita; il CONNECTACK comunica anche al client se la sua sessione è o meno persistente
3. a questo punto il client può pubblicare i messaggi; ogni messaggio deve contenere un topic e 
    un payload. Il formato del payload è dipendente dall'applicazione, può essere binario, 
    testuale o XML/JSON.
    Quest'operazione viene effettuata mediante un messaggio PUBLISH:
    - packetId (0 se Qos=0)
    - topicName
    - qos (0,1 or 2)
    - retainFlag (il broker deve salvare il messaggio come 
        ultimo valore del topic? Così un subscriber che si connette 
        in ritarrdo potrà comunque ricevere l'ultimo messaggio)
    - payload
    - dupFlag (comunica se il messaggio è un duplicato di un messaggio 
        precedente; sensato solo con QoS>0)
4. alla ricezione di un PUBLISH, il broker invia un ack se QoS>0, processa il messaggio 
	per identificare i subscriber interessati e infine glielo inoltra.
5. per potersi iscrivere ad un topic, un client deve inviare una SUBSCRIBE, questo contiene:
	- packetId
	- topic
	- QoS
	topic e QoS sono ripetuti in una lista se sono rischeste più sottoscrizioni insieme
6. il broker conferma la sottoscrizione mediante un SUBACK, questo contiene:
	- packedId
	- returnCode (uno per topic):
		- 128 = operazione fallita
		- 0, 1, 2 indicano una sottoscrizione corretta, il valore indica la QoS assegnata
7. un client può anche disiscriversi, lo può fare con un UNSUBSCRIBE; tale messaggio è come un 
	SUBSCRIBE ma senza QoS
	
### Topics

I topic non sono altro che stringhe organizzate in una gerarchia. 
Le gerarchie sono separate da `/`.

Un esempio può essere `home/firstfloor/bedroom/presence`.
L'uso delle wildcards è supportato, possiamo quindi usare:

`home/firstfloor/+/presence`
: per selezionare tutti i sensori di presenza in tutte le stanze del primo piano

`home/firstfloor/#`
: per selezionare tutti i sensori del primo piano

Alcuni topic sono riservati per le statistiche interne di MQTT e non possono essere
pubblicate dai client; tali statistiche iniziano con `$`, ma non sono 
standardizzate.

### QoS

La QoS è un accordo tra mittente e destinatario di un messaggio. 
In particolare coinvolge il canale tra P e B e quello tra S e B.

Come abbiamo già anticipato ci sono 3 livelli di QoS:

- 0 - al massimo uno
	- best effort delivery
	- messaggi senza ack del destinatario
	- messaggi non salvati nel broker, vengono solo inoltrati
	- fornisce le medesime garanzie di TCP (ovvero in caso di disconnessione 
		di una delle due parti non ci sono garanzie)
- 1 - almeno uno
	- i messaggi sono salvati nel broker fino a che non sono consegnati ai S 
		con QoS pari a 1
	- ogni messaggio è consegnato **almeno** una volta
	- i S riscpondono con ack (SUBACK)
- 2 - esattamente uno
	- QoS massima, a discapito della velocità
	- ogni messaggio viene consegnato una sola volta
	- usa un doppio two-way handshake
		- il broker
			- riceve il PUBLISH con il messaggio
			- processa il PUBLISH
			- invia il PUBREC
			- mantiene un riferimento al messaggio fino a che non riceve un PUBREL, a cui risponderà con un PUBCOMP
		- il client
			- invia il PUBLISH con il messaggio
			- attende il PUBREC, lo salva e scarta il messaggio
			- invia il PUBREL al broker per informarlo
			- attende il PUBCOMP e poi scarta il PUBREC
	- questa procedura è necessaia perchè:
		1. il primo handshake server l'invio del messaggio
		2. il secondo serve per essere in accordo a scartare lo stato
		3. il PUBREC da solo non è sufficiente in quanto se questo dovesse andare perso
            - se PUBREC È perso, il client rinvierà il messaggio
            - il broker deve mantenere un riferimento al messaggio per identificare i duplicati
            - con PUBCOMP c'è la certezza di poter scartare lo stato associato ad un messaggio

Da notare che per quanto riguarda la QoS i flussi P-B e S-B sono indipendenti, 
sia per livello di QoS che per packetId (che sono unici per client).

Inoltre i messaggi con QoS 1 o 2 sono salvati per i client offline con sessione 
persistente.

#### Casi d'uso

- 0 
    - la connessione è stabile ed affidabile
    - un singolo messaggio non è così importante e diventa obsoleto nel tempo
    - quando gli aggiornamenti sono frequenti e i messaggi diventano obsoleti 
        velocemente
    - non  c'è necessita di salvare messaggi per i client offline
- 1 
    - tutti i messaggi sono necessari ed è possibile **gestire i duplicati**
- 2 
    - tutti i messaggi sono necessari ed è **impossibile gestire i duplicati**

#### Ultime volontà e testamento

Questa funzionalità è usata per notificare agli altri client la disconnessione 
anomala di un altro client.
Il client deve comunicare al brokker il comportamento specifico da seguire 
durante la connessione; il broker salverà quindi il messaggio in questione.

Quando il broker rileva la disconnessione anomala del client in questione 
invia il messaggio precedentemente richiesto a tutti i subscriber del topic specificato 
nelle ultime volontà.

Se il client si disconnette normalmente, le ultime volontà vengono eliminate.

A questo fine il messaggio CONNECT contiene quattro campi opzionali:

- lastWillTopic: a topic
-  lastWillQoS: a QoS level (either 0,1,2)
- lastWillMessage: a string
- lastWillRetain: a boolean

Il broker invierà le ultime volontà di un client se 

- c'è un errore di rete
- il client non invia un KeepAlive in tempo
- il client chiude la connessione senza DISCONNECT
- il broker chiude la connessione con il client a causa di un errore di protocollo

### Problemi

Il bisogno di un broker centralizzato può essere limitante nelle applicazioni iot dove 
serve molta comunicazione point-to-point, l'overhead renderebbe infatti impossibile 
scalare.
Altro problema nasce dal broker, questoè infatti un single point of failure.

Abbiamo poi la scelta del protocollo a cui appoggiarsi: TCP.
Questo protocollo non è particolarmente adatto a dispositivi poco potenti in quanto:

- richiede molte più risorse di UDP
- dilata i tempi di connessione a causa dell'handshake
- le connessioni TCP non solo devono essere stabilite, ma anche mantenute attive
- tutte le motivazioni elencate impattano sulla durata della batteria

#### Alternative

##### HTTP

||MQTT|HTTP|
|---|:---:|:---:|
|pattern|publish/subscribe|client/server|
|focus of communication|single data (byte array) |single document|
|size of messages|small, with small header|large, details encoded in text|
|service levels|3|1|
|kind of interaction|1 a *|1 a 1|

##### CoAP

Constrained Application Protocol è un protocollo specializzato nel trasferimento 
via web con nodi e reti IoT. Il protocollo è pensato in particolare per la comunicazione 
Machine-2Machine (M2M).

Le sue caratteristiche sono:

- paradigma client/server
    - i server sono i sensor/attuatori
    - le applicazioni sono i client
- modello REST
- simile ad HTTP ma
    - si basa su UDP/IP
    - ha piccoli header con codifica compatta
    - fornisce un meccanismo di scoperta delle risorse

Tale protocollo è stato progettato pensando a nodi con con microcontrollori a 
8 bit, con poca ROM e poca RAM e su reti limitate come  IPv6 over Low-Power Wireless
Personal Area Networks (6LoWPANs)

- pro 
    - UDP
    - supporto al multicast (anche se com 1:1 sono preferite)
    - sicurezza
    - permette comunicazioni asincrone
- con
    - maturità dello standard 
    - reliability dei messaggi non sofisticata
    
Concludendo sia MQTT che CoAP sono idonei per applicazioni IoT, 
MQTT è preferito per dispositivi low-power, per mette poi di separare producers e 
consumers.
CoAP vanta però una sicurezza superiore a quella di MQTT.

## CoAP

### Caratteristiche principali

- header di 4 Byte
- usa UDP, SMS (supporta anche TCP)
- fornisce sicurezza tramite DTLS
- iscrizione asincrone
- protocollo built-in di discovery

CoAP è un protocollo RESTful ideale per reti e dispositivi limitati, specializzato in 
applicazioni M2M e facilmente utilizzabile tramite proxy da/a HTTP.
Si noti però che tale protocollo non è inteso per sostituire HTTP, comprimere 
HTTP e inoltre non pensato per il solo utilizzo in reti isolate d'automazione.

### Modello delle transazioni

- trasporto
	- binding UDP con siceurezza DTLS
	- possibilità di usare CoAP su SMS o TCP
- scambio di messaggi
	- semplice scambio di messaggi tra endpoint 
	- ai messaggi confermabili o non confermabili vengono risposti 
		ack o messaggi di reset
- semantica REST
	- le richieste rest sono veicolate in messaggi CoAP
	- prevede metodi, codici di ritorno e opzioni

### Header del messaggio

Ver
: versione

T
: tipo di messaggio: confermabile, non confermabile, ack, reset

TKL
: lunghezza del token, se c'è, dopo l'header

Code
: codice richiesta (1-10) o risposta (40-255)

Message ID
: id per associare le rispose alle domande

Token
: (opzionale) token

Opzioni
: 

### Caching

CoAP include un semplice modello di caching.
La cacheability è determinata dal codice di risposta associato ad un'opzione,
che funge da maschera per il codice di ritorno.

Grazie a specifiche opzioni, viene segnalato il tempo residuo della cache ed 
i controlli di validità.
Le operazioni di caching sono spesso effettuate da un proxy, che rimuove il 
carico da un nodo limitato; Questo permette anche di ridurre il carico di rete.





















































